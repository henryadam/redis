import redis
import time
import os
import mmap
from flask.ext import restful
from flask import Flask, request, redirect, url_for
from werkzeug.utils import secure_filename
from json import dumps, loads, JSONEncoder, JSONDecoder
from utils.file_helper import allowed_file
import pymssql
import uuid

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER = os.path.join(APP_ROOT, 'vault/uploads')
LOG_PATH = os.path.join(APP_ROOT, 'logs/foo.log')


#connect to redis

if os.getenv('DEVELOPMENT'):
    USE_LOCAL_REDIS = False
else:
    USE_LOCAL_REDIS = True

if USE_LOCAL_REDIS:
   POOL = redis.ConnectionPool(host='127.0.0.1', port=6379, db=0)
   REDIS = redis.StrictRedis(connection_pool=POOL)
   MSSQL = pymssql.connect('10.10.1.50', 'HenryA', '3BerGeN2$!', 'dev.gigats')
else:
   POOL = redis.ConnectionPool(host='10.10.99.4', port=6379, db=0)
   REDIS = redis.StrictRedis(connection_pool=POOL)
   MSSQL = pymssql.connect('10.10.1.50', 'HenryA', '3BerGeN2$!', 'dev.gigats')


app = Flask(__name__)
api = restful.Api(app)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['GIGATS_VAULT'] = "gigats_vault"



class GetRandomKeyFromSet(restful.Resource):
    def get(self):
        ret_dict = {}
        start = time.time()
        key = REDIS.randomkey()
        member = REDIS.srandmember(key)
        end = time.time()
        duration = end - start
        ret_dict['executionTime'] = duration
        ret_dict['key'] = key
        ret_dict['random_member'] = member
        return ret_dict

class Insert(restful.Resource):
    def get(self):
        success = True
        return success
    def put(self):
        """this method will insert hashes from gigats into the gigats vault"""
        pass

class GetTransaction(restful.Resource):
    def get(self, transaction_id):
         start = time.time()
         members = list(REDIS.smembers(transaction_id))
         response = {}
         response['duration'] = 0
         response['return_list'] = members
         response['process_success'] = True
         response['return_list_size'] = len(members)
         response['transaction_id'] = str(transaction_id)
         end = time.time()
         response['duration'] = end - start
         return response



class Upload(restful.Resource):
    def post(self):
        start = time.time()
        print request.form
        print request.files
        file = request.files.get('f')
        username = request.form.get('u')
        password = request.form.get('p')

        intersection_members = []
        path_file_to_save = None

        #set up the response payload
        success = {
            'upload_success': False,
            'process_success': False,
            'duration': 0,
            'return_list_size': 0,
            'transaction_id':0

        }
        #get a transaction id
        # cursor = MSSQL.cursor(as_dict=True)
        # cursor.execute('SELECT [emailHash], [userID], [dateCreated] FROM [dev.gigats].[dbo].[tblUser_EmailHash](nolock)')
        # hashes = cursor.fetchall()
        #
        # MSSQL.close()
        #
        # cursor = MSSQL.cursor(as_dict=True)

        transaction_id = str(uuid.uuid4()).upper()


        start_upload = time.time()
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            path_file_to_save = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(path_file_to_save)
            file.close()
            success['upload_success'] = os.path.exists(path_file_to_save)
        end_upload = time.time()

        print 'upload time' + str(end_upload - start_upload)


        if success['upload_success']:
            start_processing_file = time.time()
            with open(path_file_to_save, 'rb') as f:
               # Size 0 will read the ENTIRE file into memory!
                m = mmap.mmap(f.fileno(), 0, access=1)  # File is open read-only
                # Proceed with your code here -- note the file is already in memory
                # so "readine" here will be as fast as could be
                data = m.readline()  # one line comma delimited list
                data = map(lambda x: x.upper(), data.split(','))
            m.close()
            end_processing_file = time.time()

            print 'file processing time:' + str(end_processing_file - start_processing_file)

            vendor_vault = username + '_vault'
            vendor_intersection_key_name = str(transaction_id)
            pipeline = REDIS.pipeline()

            start_insert_to_vault = time.time()
            #add upload items to the vendor vault
            for hash in data:
                pipeline.sadd(vendor_vault,hash)
                #print REDIS.sismember(vendor_vault, hash)
            pipeline.execute()
            end_insert_to_vault = time.time()
            print 'insert_to_vault:' + str(end_insert_to_vault - start_insert_to_vault)

            #calculate the intersection and store it, we need the emailSuppressionTrackingID [transactionId]
            start_set_processing = time.time()
            intersection = REDIS.sinterstore(vendor_intersection_key_name,[str(vendor_vault),str(app.config['GIGATS_VAULT'])])
            print "intersected %s hashes" % (intersection)
            #get intersection members from vendor_intersection_key_name
            #
            #print "---" + str(intersection)
            #print "---"+ str(len(intersection_members))
            #remove intersection members from the vendor vault, that way these keys are not in play
            #for subsequent requests


            print "bout to remnove dirty from from vendor [%s] vault" % (vendor_vault)
            print "before Removal of intersection : %s" % (REDIS.scard(vendor_vault))
            #we overwrite the vendor store with the difference of the sets we just computed
            pipeline = REDIS.pipeline()
            for hash in list(REDIS.smembers(vendor_intersection_key_name)):
                pipeline.srem(vendor_vault,hash)

            pipeline.execute()

            #diff = REDIS.sdiffstore(vendor_vault,[str(vendor_vault),str(vendor_intersection_key_name)])
            print "After Removal of intersection : %s" % (REDIS.scard(vendor_vault))
            print "removed from vendor vault" + str(REDIS.scard(vendor_vault))

            end_set_processing = time.time()
            print 'set processing:' + str(end_set_processing - start_set_processing)

            start_prepare_return_list = time.time()

            success['return_list_size'] = REDIS.scard(str(vendor_intersection_key_name))
            members = list(REDIS.smembers(vendor_intersection_key_name))
            success['process_success'] = True
            success['return_list'] = members
            success['transaction_id'] = transaction_id
            end_prepare_return_list = time.time()
            print 'prepare return:' + str(end_prepare_return_list - start_prepare_return_list)

        end = time.time()
        success['duration'] = end - start
        #now that were done bg save but try it cause if another is already running this one will fail

        return success


api.add_resource(GetRandomKeyFromSet, "/get-random-key-from-set/")
api.add_resource(Upload, "/upload/")
api.add_resource(Insert, "/put-hash/")
api.add_resource(GetTransaction, "/get-transaction/<string:transaction_id>/")
#api.add_resource(VendorInitialPopulate, "/vendor_initial/")

if __name__ == '__main__':
    #import logging
    #logging.basicConfig(filename=LOG_PATH,level=logging.DEBUG)
    app.run(debug=True)

