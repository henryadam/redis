__author__ = 'henry.adam'

ALLOWED_EXTENSIONS = ['','txt','csv']

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS